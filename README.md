# Instalación de go en ububtu

```
$ sudo add-apt-repository ppa:gophers/archive
$ sudo apt update
$ sudo apt-get install golang-1.9-go
```

Exportamos las variables GOPATH y añadimos go al PATH de linux:

```
export GOPATH=/path/to/projectfolder
export GOROOT=/usr/lib/go-1.9
export PATH=$PATH:$GOROOT/bin
```

Example:

```
export GOPATH=$HOME/goprojects
```

Estructura de goprojects:

```
goprojects
  - src
      - gitlab.com
          - username
              - project1name
              - project2name
```